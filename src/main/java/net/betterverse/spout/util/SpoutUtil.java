/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.spout.util;

import java.util.HashMap;
import java.util.Map;
import net.betterverse.communities.Communities;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.WorldCoord;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.Spout;
import org.getspout.spoutapi.SpoutManager;
import org.getspout.spoutapi.gui.GenericLabel;
import org.getspout.spoutapi.gui.InGameHUD;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 *
 * @author Admin
 */
public class SpoutUtil {
	private static final Map<String, Integer> widgetTasks = new HashMap<String, Integer>();

	public static boolean isSpoutCraftEnabled(Communities plugin, Player player) {
		return plugin.isSpoutEnabled() ? SpoutManager.getPlayer(player).isSpoutCraftEnabled() : false;
	}

	public static void clearWidgets(Communities plugin, Player player) {
		if(!plugin.isSpoutEnabled()) {
			return;
		}
		try {
			// Clear SpoutCraft widgets from the player's screen
			SpoutPlayer sPlayer = Spout.getServer().getPlayer(player.getName());
			if (sPlayer != null && sPlayer.isSpoutCraftEnabled()) {
				if (widgetTasks.containsKey(player.getName())) {
					sPlayer.getMainScreen().removeWidgets(plugin);
					plugin.getServer().getScheduler().cancelTask(widgetTasks.get(player.getName()));
				}
			}
		} catch (ClassCastException e) {
			// Spout.getServer().getPlayer returned a CraftPlayer (Spout didn't load properly).
		}
	}

	public static void onPlayerEnterTerritory(Community comm, Communities plugin, Player player) {
		WorldCoord coord = WorldCoord.parseWorldCoord(player);

		if (plugin.isSpoutEnabled()) {
			SpoutPlayer sPlayer = Spout.getServer().getPlayer(player.getName());
			if (sPlayer != null && sPlayer.isSpoutCraftEnabled()) {
				final InGameHUD mainScreen = sPlayer.getMainScreen();
				// Town title
				final GenericLabel titleLabel = new GenericLabel(ChatColor.YELLOW + comm.getName());
				titleLabel.setScale(2).setHeight(1).setWidth(1).setX(mainScreen.getWidth() / 2).setY(30).setDirty(true);
				mainScreen.attachWidget(plugin, titleLabel);
				// Town description
				GenericLabel descLabel = new GenericLabel(comm.getDescription(coord));
				descLabel.setHeight(1).setWidth(1).setX(mainScreen.getWidth() - GenericLabel.getStringWidth(descLabel.getText())).setY(0).setDirty(true);
				mainScreen.attachWidget(plugin, descLabel);

				// Remove title label after 5 seconds
				int id = plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					@Override
					public void run() {
						mainScreen.removeWidget(titleLabel);
					}
				}, 100);
				widgetTasks.put(player.getName(), id);
				return;
			}
		}

		// Default to chat if SpoutCraft fails for some reason
		player.sendMessage(comm.getDescription(coord));
	}
}
