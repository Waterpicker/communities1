package net.betterverse.communities;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.betterverse.communities.command.CommunityCommand;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityChunkGroup;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CoreManager;
import net.betterverse.communities.community.WorldCoord;
import net.betterverse.communities.event.CommunityDeleteEvent;
import net.betterverse.communities.listener.CommunitiesChunkListener;
import net.betterverse.communities.listener.CommunitiesListenerForCore;
import net.betterverse.communities.listener.StargateListener;
import net.betterverse.communities.util.Configuration;
import net.betterverse.spout.util.SpoutUtil;


import net.milkbowl.vault.economy.Economy;
import org.apache.commons.lang.StringUtils;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.dynmap.DynmapAPI;
import org.dynmap.markers.AreaMarker;
import org.dynmap.markers.MarkerSet;

public class Communities extends JavaPlugin implements Listener {

	private final Map<String, CommunityChunkGroup> chunkGroups = new HashMap<String, CommunityChunkGroup>();
	private final Map<String, Community> communities = new HashMap<String, Community>();
	private final Map<String, CommunityPlayer> players = new HashMap<String, CommunityPlayer>();
	private final Map<String, Location> fromCache = new HashMap<String, Location>();
	private Configuration config;
	private Economy economy;
	private DynmapAPI dynmap;
	private boolean spoutEnabled;
	private MarkerSet markerSet;
	private CoreManager core;

	@Override
	public void onDisable() {
		//TODO: Either remove (would be redundant) or move to core.unload()
		// Save communities and players to disk
		for (String community : communities.keySet()) {
			communities.get(community).save();
		}
		for (String player : players.keySet()) {
			players.get(player).save();
		}

		disableDynmap();

		getServer().getScheduler().cancelTasks(this);

		core.unloadData();

		log(toString() + " disabled.");
	}

	@Override
	public void onEnable() {
		core = new CoreManager(getDataFolder());
		try {
			core.load();
		} catch (Exception e) {
			getLogger().log(Level.SEVERE, "Error loading Communities core.", e);
			return;
		}

		PluginManager pm = getServer().getPluginManager();

		// Disable if Vault is not found
		if (!setupEconomy()) {
			log("Could not hook into Vault! Disabling...");
			pm.disablePlugin(this);
		}

		config = new Configuration(this);
		config.load();

		getCommand("communities").setExecutor(new CommunityCommand(this));

		pm.registerEvents(this, this);
		pm.registerEvents(new CommunitiesListenerForCore(this), this);
		pm.registerEvents(new CommunitiesChunkListener(this), this);

		log(toString() + " enabled.");

		getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			@Override
			public void run() {
				for (Player player : getServer().getOnlinePlayers()) {
					if (!fromCache.containsKey(player.getName())) {
						fromCache.put(player.getName(), player.getLocation());
						continue;
					}
					onPlayerMove(player, player.getLocation(), fromCache.get(player.getName()));
					fromCache.put(player.getName(), player.getLocation());
				}
			}
		}, 20L, 20L);


		// Check if a dependancy is already loaded. If not, listen for it being enabled later on.
		if (pm.isPluginEnabled("Stargate")) {
			onEnableStargate();
		}

		// Check for dependencies after all plugins have loaded. Unfortunately this is necessary as even with
		// soft-depends, plugin load order is not always as expected.
		getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
			@Override
			public void run() {
				try {
					PluginManager pm = getServer().getPluginManager();

					if (pm.isPluginEnabled("dynmap")) {
						enableDynmap();
					}
					if (pm.isPluginEnabled("Spout")) {
						spoutEnabled = true;
					}

					// Load communities from disk last
					loadCommunities();
				} catch (Exception ex) {
					Logger.getLogger(Communities.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		});
	}

	@Override
	public String toString() {
		return String.format("%s v%s [Written by: %s]",
				getDescription().getName(),
				getDescription().getVersion(),
				StringUtils.join(getDescription().getAuthors(), ", "));
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();

		// Convert the Player into a CommunityPlayer if it hasn't been converted
		if (!players.containsKey(player.getName())) {
			loadPlayer(player.getName());
		}

		CommunityPlayer comPlayer = players.get(player.getName());
		if (comPlayer.getCommunity() != null) {
			comPlayer.getCommunity().onJoin(event);
		}
	}

	@EventHandler
	public void onPluginDisable(PluginDisableEvent event) {
		// In case a plugin disables while Communities is enabled
		String plugin = event.getPlugin().getName();
		if (plugin.equals("dynmap")) {
			disableDynmap();
		} else if (plugin.equals("Spout")) {
			spoutEnabled = false;
		}
	}

	@EventHandler
	public void onPluginEnable(PluginEnableEvent event) {
		// In case a plugin enables while Communities has already been enabled
		String plugin = event.getPlugin().getName();
		if (plugin.equals("dynmap")) {
			enableDynmap();
		} else if (plugin.equals("Spout")) {
			spoutEnabled = true;
		} else if (plugin.equals("Stargate")) {
			onEnableStargate();
		}
	}

	public Configuration config() {
		return config;
	}

	public Community createCommunity(String name, World world, Location location) {
		// Initialize a new community with an initial chunk
		Community community = new Community(this, name, world, location);
		communities.put(name, community);

		return community;
	}

	public AreaMarker createOrFindMarker(String name, String world, double[] x, double[] z) {
		AreaMarker marker = markerSet.findAreaMarkerByLabel(name);
		if (marker == null) {
			marker = markerSet.createAreaMarker("communities.towns." + name, name, false, world, x, z, false);
		}

		return marker;
	}

	public Economy economy() {
		return economy;
	}

	public CommunityPlayer fromBukkitPlayer(String player) {
		if (!players.containsKey(player)) {
			loadPlayer(player);
		}
		return players.get(player);
	}

	public CommunityChunkGroup getChunkGroup(String name, Community community) {
		if (!chunkGroups.containsKey(name)) {
			chunkGroups.put(name, new CommunityChunkGroup(this, name, community));
		}

		return chunkGroups.get(name);
	}

	public Community getChunkOwner(WorldCoord coord) {
		for (Community community : communities.values()) {
			if (community.ownsChunk(coord)) {
				return community;
			}
		}

		return null;
	}

	public Community getCommunity(String name) {
		return communities.get(name);
	}

	public Collection<Community> getCommunities() {
		return communities.values();
	}

	public boolean isChunkOwned(WorldCoord coord) {
		for (Community community : communities.values()) {
			if (community.ownsChunk(coord)) {
				return true;
			}
		}

		return false;
	}

	public boolean isSpoutEnabled() {
		return spoutEnabled;
	}

	public void log(Level level, String message) {
		getServer().getLogger().log(level, "[Communities] " + message);
	}

	public void log(String message) {
		log(Level.INFO, message);
	}

	public boolean removeCommunity(String name) {
		Community community = getCommunity(name);
		if (community != null) {
			CommunityDeleteEvent event = new CommunityDeleteEvent(community);
			getServer().getPluginManager().callEvent(event);

			community.remove();
			communities.remove(community.getName());

			// Delete town save file
			File delete = new File(getDataFolder() + File.separator + "communities", name + ".yml");
			delete.delete();
			return true;
		}

		return false;
	}

	private void disableDynmap() {
		// Delete dynmap marker
		if (markerSet != null) {
			markerSet.deleteMarkerSet();
			markerSet = null;
		}
	}

	private void enableDynmap() {
		dynmap = (DynmapAPI) getServer().getPluginManager().getPlugin("dynmap");

		markerSet = dynmap.getMarkerAPI().createMarkerSet("communities.towns", "Communities", null, false);

		for (Community community : communities.values()) {
			community.render();
		}
	}

	private void onEnableStargate() {
		getServer().getPluginManager().registerEvents(new StargateListener(this), this);
	}

	private void loadCommunities() throws Exception {
		// Create communities directory if it does not already exist
		File communityFolder = new File(getDataFolder(), "communities");
		if (!communityFolder.exists()) {
			communityFolder.mkdirs();
			log("Created 'communities' directory to store community data.");
		}

		for (File communityFile : communityFolder.listFiles()) {
			if (!communityFile.isDirectory()) {
				// Construct and load a Community from the file
				String communityName = communityFile.getName().replace(".yml", "");
				if (communityName.isEmpty()) {
					continue;
				}
				// System.out.println(String.format("Loading Community(name = '%s', communityName = '%s')", name, communityName));
				communities.put(communityName, new Community(this, communityName));
			}
		}

		// Load communities after all Community objects have been created
		for (Community community : communities.values()) {
			try {
				community.load();
			} catch (Exception e) {
				throw new Exception(String.format("Error loading community '%s'.", community.getName()), e);
			}
		}

		log("Loaded " + communities.values().size() + " communities.");
	}

	private void loadPlayer(String name) {
		File playerFolder = new File(getDataFolder(), "players");
		if (!playerFolder.exists()) {
			// Create the players directory if it does not already exist
			playerFolder.mkdirs();
			log("Created 'players' directory to store player data.");
		}
		File playerFile = new File(playerFolder, name + ".yml");
		if (!playerFile.exists()) {
			// Create a new YAML file to store the player's data if it does not already exist
			try {
				playerFile.createNewFile();
			} catch (IOException e) {
				log("Unable to create new save file for the player '" + name + "'. " + e.getMessage());
			}
		}

		// Construct and load a CommunityPlayer from the file
		CommunityPlayer communityPlayer = new CommunityPlayer(this, name);
		communityPlayer.load();
		players.put(name, communityPlayer);
	}

	private void onPlayerMove(Player player, Location toLoc, Location fromLoc) {
		if (toLoc.getChunk().equals(fromLoc.getChunk())) {
			return;
		}

		WorldCoord toCoord = WorldCoord.parseWorldCoord(toLoc);
		WorldCoord fromCoord = WorldCoord.parseWorldCoord(fromLoc);

		if (isChunkOwned(fromCoord)) {
			if (!isChunkOwned(toCoord) || !getChunkOwner(fromCoord).getName().equals(getChunkOwner(toCoord).getName())) {
				// The player has left a community
				getChunkOwner(fromCoord).onPlayerExitTerritory(player);
			}
		}

		if (isChunkOwned(toCoord)) {
			if (!isChunkOwned(fromCoord) || !getChunkOwner(toCoord).getName().equals(getChunkOwner(fromCoord).getName())) {
				// The player has entered a new community
				if(isSpoutEnabled()) {
					SpoutUtil.onPlayerEnterTerritory(getChunkOwner(toCoord), this, player);
				} else {
					player.sendMessage(getChunkOwner(toCoord).getDescription(toCoord));
				}
			}
		}
	}

	private boolean setupEconomy() {
		RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(Economy.class);
		if (economyProvider != null) {
			economy = economyProvider.getProvider();
		}

		return economy != null;
	}

	public CoreManager getCore() {
		return core;
	}
}
