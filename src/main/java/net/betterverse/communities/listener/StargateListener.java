/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.listener;

import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import net.TheDgtl.Stargate.Gate;
import net.TheDgtl.Stargate.Portal;
import net.TheDgtl.Stargate.event.StargateAccessEvent;
import net.TheDgtl.Stargate.event.StargateCreateEvent;
import net.TheDgtl.Stargate.event.StargateDestroyEvent;
import net.betterverse.communities.Communities;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.WorldCoord;
import net.betterverse.communities.config.StargatesConfig;
import net.betterverse.communities.controller.CommunityController;
import net.betterverse.communities.controller.CommunityStargateController;
import net.betterverse.communities.event.CommunityDeleteEvent;
import net.betterverse.communities.event.CommunityUnclaimChunkEvent;
import net.betterverse.communities.model.CommunityStargate;
import org.apache.commons.io.FilenameUtils;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

/**
 *
 * @author Admin
 */
public class StargateListener implements Listener {

	Communities plugin;

	public StargateListener(Communities plugin) {
		this.plugin = plugin;
	}

	private StargatesConfig config() {
		return plugin.getCore().getCommunityStargateConfig();
	}

	@EventHandler
	public void onStargateCreate(StargateCreateEvent event) {
		Portal portal = event.getPortal();
		String portalNetworkName = portal.getNetwork();
		List<String> userPortalNetwork = Portal.getNetwork(portalNetworkName);

		// Check if we're building a Town gate
		if (!event.getLine(StargatesConfig.COMMUNITY_STARGATE_IDENTIFIER_SIGN_LINE_ID).equalsIgnoreCase(config().communityStargateIndentifier)) {
			// Not a town stargate
			if (userPortalNetwork == null || userPortalNetwork.isEmpty()) {
				// New network
				if (config().matchesCommunityNetworkNamePattern(portalNetworkName)) {
					// Trying to create a new network that matches a current or future community network.
					event.setDenyReason(String.format("Cannot create a network named like '%s'.", config().communityStargateNetworkName));
					event.setDeny(true);
					return;
				}
			}

			return;
		}


		Block sign = portal.getSign();

		// The sign is the key vector for checking where it belongs.
		WorldCoord signCoord = WorldCoord.parseWorldCoord(sign);

		// Check if this build is in a claimed area
		if (!plugin.isChunkOwned(signCoord)) {
			// Not in owned land.
			event.setDenyReason("This network name is reserved for creating town stargates.");
			event.setDeny(true);
			return;
		}

		Community signCommunity = plugin.getChunkOwner(signCoord);

		Player player = event.getPlayer();
		CommunityPlayer communityPlayer = plugin.fromBukkitPlayer(player.getName());
		Community playerCommunity = communityPlayer.getCommunity();

		if (playerCommunity == null) {
			// Player not in community.
			event.setDenyReason("Must belong to a community to make town stargates.");
			event.setDeny(true);
			return;
		}

		if (!signCommunity.getName().equals(playerCommunity.getName())) {
			// Portal made in another community.
			event.setDenyReason("Cannot make town stargates in other towns.");
			event.setDeny(true);
			return;
		}

		// Established that both communities are the same.
		Community community = playerCommunity;

		// Check if player has permission to make town portals.
		if (!communityPlayer.isLeader()) {
			// Portal made in another community.
			event.setDenyReason("You don't have permission to build town gates (must be assistant rank or higher).");
			event.setDeny(true);
			return;
		}

		// Check that the current gate layout is allowed as a town gate.
		Gate gate = portal.getGate();
		String gateName = FilenameUtils.removeExtension(gate.getFilename());
		if (!config().allowedGateNames.contains(gateName)) {
			event.setDenyReason("That gate layout is not allowed as a town gate.");
			event.setDeny(true);
			return;
		}

		// Check that the community is big enough for a stargate.
		if (community.getSize() < config().minCommunitySize) {
			event.setDenyReason("There are not enough residents in your town.");
			event.setDeny(true);
			return;
		}



		CommunityController communityController = plugin.getCore().getCommunityController();
		//TODO Refactor when moved to sql everywhere.
		net.betterverse.communities.model.Community _community = communityController.getByName(community.getName());
		if (_community == null) {
			_community = communityController.create(community.getName());
		}
		String networkName = communityController.getCommunityPortalNetwork(_community);

		// Check that the community has room in it's network.
		List<String> communityNetworkPortalNames = Portal.getNetwork(networkName);
		if (communityNetworkPortalNames != null) {
			// Network already exists

			if (communityNetworkPortalNames.size() >= config().maxCommunityNetworkSize) {
				event.setDenyReason("Your community's network is too large.");
				event.setDeny(true);
				return;
			}
		}

		portal.setDestination("");
		portal.setNetwork(networkName);
		portal.setNoNetwork(true);

		CommunityStargate communityStargate = new CommunityStargate();
		communityStargate.communityId = _community.id;
		communityStargate.portalName = portal.getName();
		communityStargate.networkName = networkName;

		CommunityStargateController stargateController = plugin.getCore().getCommunityStargateController();
		try {
			stargateController.validate(communityStargate);
		} catch (Exception e) {
			// Missmatch between plugins found. Trust the Stargate DB over ours.

			// Delete the old record if it exists.
			CommunityStargate oldCommunityStargate = stargateController.getByPortalName(communityStargate.portalName, communityStargate.networkName);
			if (oldCommunityStargate != null) {
				plugin.log(Level.WARNING, String.format("Missmatch between plugins found. Trust the Stargate DB over ours. Deleting %s.", oldCommunityStargate));
				stargateController.delete(oldCommunityStargate);
			}
		}

		stargateController.register(communityStargate);

	}

	@EventHandler
	public void onStargateDestroy(StargateDestroyEvent event) {
		Portal portal = event.getPortal();

		CommunityStargate communityStargate = plugin.getCore().getCommunityStargateController().getByPortalName(portal.getName(), portal.getNetwork());
		if (communityStargate == null) {
			// Not a community portal
			return;
		}

		//TODO Refactor when moved to sql everywhere.
		net.betterverse.communities.model.Community _community = plugin.getCore().getCommunityController().getById(communityStargate.communityId);
		Community portalCommunity = plugin.getCommunity(_community.name);

		Player player = event.getPlayer();

		if (true) { //TODO Admin override
			CommunityPlayer communityPlayer = plugin.fromBukkitPlayer(player.getName());
			Community playerCommunity = communityPlayer.getCommunity();

			if (playerCommunity == null) {
				// Player not in community.
				event.setDenyReason("Must belong to a community to destroy town stargates.");
				event.setDeny(true);
				return;
			}

			if (!portalCommunity.getName().equals(playerCommunity.getName())) {
				// Portal made in another community.
				event.setDenyReason("Cannot destroy town stargates in other towns.");
				event.setDeny(true);
				return;
			}

			// Established that both communities are the same.

			// Check if player has permission to make town portals.
			if (!communityPlayer.isLeader()) {
				// Portal made in another community.
				event.setDenyReason("You don't have permission to destroy town gates (must be assistant rank or higher).");
				event.setDeny(true);
				return;
			}
		}

		plugin.getCore().getCommunityStargateController().delete(communityStargate);
	}

	@EventHandler
	public void onStargateAccess(StargateAccessEvent event) {
		Portal portal = event.getPortal();

		CommunityStargate communityStargate = plugin.getCore().getCommunityStargateController().getByPortalName(portal.getName(), portal.getNetwork());
		if (communityStargate == null) {
			// Not a community portal
			return;
		}

		//TODO Refactor when moved to sql everywhere.
		net.betterverse.communities.model.Community _community = plugin.getCore().getCommunityController().getById(communityStargate.communityId);
		Community portalCommunity = plugin.getCommunity(_community.name);

		Player player = event.getPlayer();

		if (true) { //TODO Admin override
			if (!portalCommunity.isMember(player.getName())) {
				// Portal made in another community.
				event.setDeny(true);
				return;
			}
		}
	}

	@EventHandler // Run before 
	public void onCommunityDelete(CommunityDeleteEvent event) {
		String communityName = event.getCommunity().getName();
		CommunityController communityController = plugin.getCore().getCommunityController();


		net.betterverse.communities.model.Community community = communityController.getByName(communityName);

		if (community == null) {
			return;
		}

		List<CommunityStargate> communityStargates = communityController.getCommunityStargates(community);

		// Destroy all stargates.
		for (CommunityStargate communityStargate : communityStargates) {
			Portal portal = Portal.getByName(communityStargate.portalName, communityStargate.networkName);
			if (portal != null) {
				portal.unregister(true);
			}
		}

		// Delete all references to stargates.
		CommunityStargateController stargateController = plugin.getCore().getCommunityStargateController();
		stargateController.deleteAllBelongingTo(community);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onUnclaimChunk(CommunityUnclaimChunkEvent event) {
		// Assume That the selection only contains community stargates from the community specified in the task.

		String communityName = event.getUnclaimTask().getCommunity().getName();
		Set<WorldCoord> selection = event.getUnclaimTask().getSelection();
		CommunityController communityController = plugin.getCore().getCommunityController();
		CommunityStargateController stargateController = plugin.getCore().getCommunityStargateController();

		net.betterverse.communities.model.Community community = communityController.getByName(communityName);

		if (community == null) {
			return;
		}

		List<CommunityStargate> communityStargates = communityController.getCommunityStargates(community);

		// Destroy all stargates.
		for (CommunityStargate communityStargate : communityStargates) {
			Portal portal = Portal.getByName(communityStargate.portalName, communityStargate.networkName);
			if (portal != null) {
				Block sign = portal.getSign();

				// The sign is the key vector for checking where it belongs.
				WorldCoord signCoord = WorldCoord.parseWorldCoord(sign);

				if (selection.contains(signCoord)) {
					portal.unregister(true);
					stargateController.delete(communityStargate);
				}
			}
		}
	}
}
