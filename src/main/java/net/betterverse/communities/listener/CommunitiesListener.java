/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.listener;

import net.betterverse.bukkit.BukkitUtil;
import net.betterverse.communities.Communities;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.event.CommunityCreateEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 *
 * @author Admin
 */
public class CommunitiesListener implements Listener {

	Communities plugin;

	public CommunitiesListener(Communities plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onCommunityCreate(CommunityCreateEvent event) {
		try {
			// Check if a community already exists with this name.
			if (plugin.getCommunity(event.getCommunityName()) != null) {
				throw new Exception(String.format("The town '%s' already exists.", event.getCommunityName()));
			}

			// Check if this location is already claimed
			if (plugin.isChunkOwned(event.getInitialCoord())) {
				throw new Exception("You cannot create a town here. Another town already owns this chunk.");
			}

			// Check if sender already belongs to a town.
			if (event.getSender() != null && event.getSender() instanceof Player) {
				Player player = (Player) event.getSender();
				CommunityPlayer communityPlayer = plugin.fromBukkitPlayer(player.getName());
				if (communityPlayer.getCommunity() != null) {
					throw new Exception("You already are part of a town. You must leave it before creating a new town.");
				}
			}


		} catch (Exception e) {
			BukkitUtil.sendTo(event.getSender(), e.getMessage());
			event.setCancelled(true);
		}
	}
}