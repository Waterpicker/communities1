/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.listener;

import net.betterverse.communities.Communities;
import net.betterverse.communities.event.CommunityCreateEvent;
import net.betterverse.communities.event.CommunityDeleteEvent;
import net.betterverse.communities.model.Community;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

/**
 *
 * @author Admin
 */
public class CommunitiesListenerForCore implements Listener {

	Communities plugin;

	public CommunitiesListenerForCore(Communities plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onCommunityCreate(CommunityCreateEvent event) {
		// TODO Refactor after putting SQL everywhere
		String communityName = event.getCommunityName();

		Community community = plugin.getCore().getCommunityController().getByName(communityName);
		if (community != null) {
			//TODO Cleanup existing data
			return;
		}
	}

	// For now, since the event is triggered in a class that's annoying to get the core, we delete the community after all others have been notified.
	@EventHandler(priority = EventPriority.MONITOR)
	public void onCommunityDelete(CommunityDeleteEvent event) {
		// TODO Refactor after putting SQL everywhere
		String communityName = event.getCommunity().getName();
		Community community = plugin.getCore().getCommunityController().getByName(communityName);

		if (community != null) {
			// TODO Refactor after putting SQL everywhere
			plugin.getCore().getCommunityController().delete(community);
		}
	}
}
