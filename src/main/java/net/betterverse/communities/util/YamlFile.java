package net.betterverse.communities.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;

import net.betterverse.communities.Communities;

import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class YamlFile {

	private final File file;
	private final FileConfiguration config;
	private boolean saveOnSet = true;

	public YamlFile(Communities plugin, File file, String name) {
		this.file = file;
		config = new YamlConfiguration();
		name += ".yml";

		if (!file.exists()) {
			file.getParentFile().mkdirs();
			try {
				file.createNewFile();
				plugin.log("Created new file '" + name + "'.");
			} catch (IOException e) {
				plugin.log(Level.SEVERE, "Could not create file '" + name + "'!");
				e.printStackTrace();
			}
		}

		load();
	}

	public Configuration get() {
		return config;
	}

	public ConfigurationSection getSection(String key) {
		ConfigurationSection section = config.getConfigurationSection(key);
		if (section == null) {
			section = config.createSection(key);
		}

		return section;
	}

	public Object getValue(String key, Object def) {
		try {
			return config.get(key, def);
		} catch (RuntimeException e) {
			throw new RuntimeException(String.format("Error performing YamlFile(%s).getValue(%s, %s).", file.getPath(), key, def), e);
		}
	}

	public void load() {
		try {
			config.load(file);
		} catch (Exception e) {
			new Exception(String.format("Error loading file '%s'.", file.getPath()), e).printStackTrace();
		}
	}

	public void save() {
		try {
			config.save(file);
		} catch (IOException e) {
			new Exception(String.format("Error saving file '%s'.", file.getPath()), e).printStackTrace();
		}
	}

	public void set(String key, Object value) {
		try {
			config.set(key, value);
		} catch (RuntimeException e) {
			throw new RuntimeException(String.format("Error performing YamlFile(%s).set(%s, %s).", file.getPath(), key, value), e);
		}
		if (isSaveOnSet()) {
			save();
		}
	}

	public void setSaveOnSet(boolean saveOnSet) {
		this.saveOnSet = saveOnSet;
	}

	public boolean isSaveOnSet() {
		return saveOnSet;
	}

	public boolean isSet(String key) {
		return config.isSet(key);
	}
}
