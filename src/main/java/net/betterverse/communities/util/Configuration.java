package net.betterverse.communities.util;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;

import net.betterverse.communities.Communities;

public class Configuration {

	private final YamlFile file;

	public Configuration(Communities plugin) {
		this.file = new YamlFile(plugin, new File(plugin.getDataFolder(), "config.yml"), "config");
	}

	public boolean areExplosionsAllowed() {
		return file.get().getBoolean("towns.explosions");
	}

	public boolean areHomesEnabled() {
		return file.get().getBoolean("towns.homes.enabled");
	}

	public boolean canAssignPermission(String permission) {
		return file.get().getStringList("towns.valid-chunk-permissions").contains(permission);
	}

	public boolean canFireSpread() {
		return file.get().getBoolean("towns.fire-spread");
	}

	public boolean canInteractWith(Material material) {
		String name = "";
		switch (material) {
			case STONE_BUTTON:
				name = "buttons";
				break;
			case CHEST:
				name = "chests";
				break;
			case DISPENSER:
				name = "dispensers";
				break;
			case FURNACE:
				name = "furnaces";
				break;
			case LEVER:
				name = "levers";
				break;
			case IRON_DOOR:
			case IRON_DOOR_BLOCK:
			case WOODEN_DOOR:
				name = "doors";
				break;
			default:
				return true;
		}

		return file.get().getBoolean("towns.interact-with." + name);
	}

	public boolean canMobSpawn(EntityType entity) {
		return file.get().getStringList("towns.allowed-mobs").contains(entity.name());
	}

	public boolean canPvP() {
		return file.get().getBoolean("towns.pvp");
	}

	public String chatPrefix(String community, String displayName) {
		return file.get().getString("towns.chat-prefix").replace('&', '§').replace("<community>", community).replace("<display-name>", displayName);
	}

	public List<String> getAllowedBlocks() {
		return file.get().getStringList("towns.allowed-blocks");
	}

	public double getChunkCost(int numberOfChunks) {
		return file.get().getDouble("chunk-costs.initial-cost") + ((numberOfChunks - 1) * file.get().getDouble("chunk-costs.purchase-increment"));
	}

	public double getInitialCommunityBalance() {
		return file.get().getDouble("towns.initial-balance");
	}

	public long getInviteExpiryMillis() {
		long expiry = 0;
		// Hours
		expiry += file.get().getInt("invite-expiry.hours") * 3600000;
		// Minutes
		expiry += file.get().getInt("invite-expiry.minutes") * 60000;
		// Seconds
		expiry += file.get().getInt("invite-expiry.seconds") * 1000;

		return expiry;
	}

	public double getTax(int numberOfChunks) {
		return file.get().getDouble("chunk-costs.base-tax") + ((numberOfChunks - 1) * file.get().getDouble("chunk-costs.tax-increment"));
	}

	public double getHomePrice() {
		return file.get().getDouble("towns.homes.price");
	}

	public boolean isColorChangeEnabled() {
		return file.get().getBoolean("dynmap.enable-town-colors");
	}

	public boolean isValidChunkClaimRadius(int radius) {
		return radius <= file.get().getInt("towns.max-claim-chunk-radius");
	}

	public void load() {
		file.load();

		// Add defaults
		Map<String, Object> defaults = new HashMap<String, Object>();
		defaults.put("chunk-costs.initial-cost", 2000);
		defaults.put("chunk-costs.purchase-increment", 50);
		defaults.put("chunk-costs.base-tax", 250);
		defaults.put("chunk-costs.tax-increment", 10);
		defaults.put("dynmap.enable-town-colors", true);
		defaults.put("invite-expiry.hours", 0);
		defaults.put("invite-expiry.minutes", 1);
		defaults.put("invite-expiry.seconds", 0);
		defaults.put("towns.allowed-blocks", Arrays.asList("LEVER", "LADDER", "TORCH"));
		defaults.put("towns.allowed-mobs", Arrays.asList("CHICKEN", "COW", "PIG", "SHEEP", "SQUID"));
		defaults.put("towns.chat-prefix", "&7[&6<community>&7] [&f<display-name>&7]:&f ");
		defaults.put("towns.max-claim-chunk-radius", 100);
		defaults.put("towns.fire-spread", false);
		defaults.put("towns.explosions", false);
		defaults.put("towns.homes.enabled", true);
		defaults.put("towns.homes.price", 10.0);
		defaults.put("towns.initial-balance", 0.0);
		defaults.put("towns.interact-with.buttons", true);
		defaults.put("towns.interact-with.chests", true);
		defaults.put("towns.interact-with.dispensers", true);
		defaults.put("towns.interact-with.doors", true);
		defaults.put("towns.interact-with.furnaces", true);
		defaults.put("towns.interact-with.levers", true);
		defaults.put("towns.pvp", true);
		defaults.put("towns.valid-chunk-permissions", Arrays.asList("compasswarp.use"));

		for (Entry<String, Object> entry : defaults.entrySet()) {
			if (!file.get().contains(entry.getKey())) {
				file.get().set(entry.getKey(), entry.getValue());
			}
		}

		file.save();
	}
}
