package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.*;

@AnnotatedCommand(name = "monsters", usage = "monsters", desc = "Toggle monster spawning in a chunk", permission = "communities.normal.monsters", max = 1, community = true, rank = CommunityRank.MAYOR)
public class FlagMonsterSpawn extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Player pSender = (Player) sender;
		Community community = player.getCommunity();
		WorldCoord coord = WorldCoord.parseWorldCoord(pSender);
		if (community.ownsChunk(coord)) {
			CommunityChunk comChunk = community.getOwnedChunk(coord);
			boolean monsters = false;
			if (comChunk.hasGroup()) {
				monsters = comChunk.getGroup().toggleMonsters();
			} else {
				monsters = comChunk.toggleMonsters();
			}
			pSender.sendMessage(ChatColor.GREEN + "Monsters will " + (monsters ? "now" : "no longer") + " spawn in the current chunk.");
		} else {
			throw new CommandException("You must be within your town's territory to flag monster spawns.");
		}
	}
}
