package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;

@AnnotatedCommand(name = "mayor", usage = "mayor <player>", desc = "Set the mayor of your town", permission = "communities.normal.mayor", min = 2, max = 2, community = true, rank = CommunityRank.MAYOR)
public class SetMayor extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Community community = player.getCommunity();
		if (community.isMember(args[1])) {
			player.setRank(CommunityRank.MEMBER);
			community.setMayor(plugin.fromBukkitPlayer(args[1]));
			sender.sendMessage(ChatColor.GREEN + "You have changed the mayor of the town to " + ChatColor.YELLOW + args[1] + ChatColor.GREEN + ". You are no longer the mayor.");

			// Notify the new mayor if online
			Player mayor = plugin.getServer().getPlayer(args[1]);
			if (mayor != null) {
				mayor.sendMessage(ChatColor.GREEN + "You have been promoted to mayor by " + ChatColor.YELLOW + sender.getName() + ChatColor.GREEN + "!");
			}
		} else {
			throw new CommandException(args[1] + " is not a member of your town.");
		}
	}
}
