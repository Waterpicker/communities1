package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.CommunityPlayer;

@AnnotatedCommand(name = "leave", usage = "leave", desc = "Leave a town", permission = "communities.normal.leave", max = 1, community = true)
public class LeaveTown extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		if (!player.isMayor()) {
			sender.sendMessage(ChatColor.GREEN + "You have left " + ChatColor.YELLOW + player.getCommunity().getName() + ChatColor.GREEN + "!");
			player.getCommunity().removePlayer(sender.getName());
		} else {
			throw new CommandException("You cannot leave the town as mayor. Use '/coms mayor <player>' to set a new mayor before leaving.");
		}
	}
}
