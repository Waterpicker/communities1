package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.CommunityPlayer;

@AnnotatedCommand(name = "reload", usage = "reload", desc = "Reload Communities", permission = "communities.admin.reload", max = 1, server = true)
public class Reload extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		sender.sendMessage(ChatColor.GRAY + "Reloading Communities...");
		plugin.config().load();
		sender.sendMessage(ChatColor.GREEN + "Reloaded Communities!");
	}
}
