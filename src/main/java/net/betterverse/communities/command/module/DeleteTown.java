package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.CommunityPlayer;

@AnnotatedCommand(server = true, name = "delete", usage = "delete <town>", desc = "Permanently remove a town", permission = "communities.admin.delete", min = 2, max = 2)
public class DeleteTown extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		if (plugin.getCommunity(args[1]) != null) {
			if (plugin.removeCommunity(args[1])) {
				sender.sendMessage(ChatColor.GREEN + "You have permanently removed " + ChatColor.YELLOW + args[1] + ChatColor.GREEN + ".");
			} else {
				throw new CommandException("Could not remove community!");
			}
		} else {
			throw new CommandException("'" + args[1] + "' is not a valid town.");
		}
	}
}
