package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;

@AnnotatedCommand(name = "sethome", usage = "sethome", desc = "Set your town's home", permission = "communities.normal.sethome", max = 1, community = true)
public class SetHome extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Player pSender = (Player) sender;
		Community community = player.getCommunity();
		Location location = pSender.getLocation();
		if (community.isLocationWithinBorder(location)) {
			community.setHomeLocation(pSender.getLocation());
			pSender.sendMessage(ChatColor.GREEN + "Set your town's home location to your current position.");
		} else {
			throw new CommandException("You must be within your town's territory to set the home location.");
		}
	}

	@Override
	public boolean canExecute() {
		return plugin.config().areHomesEnabled();
	}
}
