package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.command.CommandUsageException;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;

@AnnotatedCommand(name = "edit", usage = "edit <break|place>", desc = "Edit placeable and breakable blocks", permission = "communities.normal.blockeditor", min = 2, max = 2, community = true, rank = CommunityRank.MAYOR)
public class ToggleBlockEditor extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Community community = player.getCommunity();
		boolean enter = false;
		String editor = "";
		if (!community.isPlayerInBlockEditor(sender.getName())) {
			if (args[1].equalsIgnoreCase("break")) {
				// Player is toggling breakable editor
				enter = community.toggleBreakEditor(sender.getName());
				editor = "break";
			} else if (args[1].equalsIgnoreCase("place")) {
				// Player is toggling placeable editor
				enter = community.togglePlacementEditor(sender.getName());
				editor = "placement";
			} else {
				throw new CommandUsageException();
			}

			sender.sendMessage(ChatColor.GREEN + "You have " + (enter ? "entered" : "exited") + " the " + editor + " editor.");
			if (enter) {
				sender.sendMessage(ChatColor.YELLOW + "Left-click" + ChatColor.GREEN + " to add blocks. " + ChatColor.YELLOW + "Right-click" + ChatColor.GREEN + " to remove blocks.");
			}
		} else {
			throw new CommandException("You are already in a block editor. Toggle out of it using '/coms edit <break|place>'.");
		}
	}
}
