package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;

@AnnotatedCommand(name = "description", usage = "description <text>", desc = "Set the town description", permission = "communities.normal.description", min = 2, community = true, rank = CommunityRank.MAYOR)
public class Description extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		StringBuilder text = new StringBuilder(args[1]);
		if (args.length > 2) {
			text.append(" ");
			for (int i = 2; i < args.length; i++) {
				// Don't append a space if it is the last word in the description
				text.append(args[i] + (i + 1 < args.length ? " " : ""));
			}
		}

		String built = text.toString();
		Community community = player.getCommunity();
		community.setDescription(built);
		sender.sendMessage(ChatColor.GREEN + "Changed town description to: " + ChatColor.RESET + built);
	}
}
