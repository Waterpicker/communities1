/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.command.module;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.CommunityChunk;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;
import net.betterverse.communities.community.WorldCoord;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author ZNickq
 */
@AnnotatedCommand(name = "unsell", usage = "unsell", desc = "Remove chunk from sale", permission = "communities.normal.unsell", min = 1, max = 1, community = true, rank = CommunityRank.MAYOR)
public class UnSell extends CommandModule{

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		WorldCoord coord = WorldCoord.parseWorldCoord((Player) sender);
		CommunityChunk chunk = player.getCommunity().getOwnedChunk(coord);
		try {
			chunk.setOnSale(0);
			sender.sendMessage(ChatColor.GREEN+"Unsold chunk!");
		} catch(Exception ex) {
			sender.sendMessage(ChatColor.RED+args[1]+" does not seem to be a number!");
		}
	}
	
}
