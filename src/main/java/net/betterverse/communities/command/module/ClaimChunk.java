package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;
import net.betterverse.communities.task.ClaimMultiTask;
import net.betterverse.communities.task.ClaimSingleTask;
import net.betterverse.communities.task.ClaimTask;
import org.bukkit.Bukkit;

@AnnotatedCommand(name = "claim", usage = "claim (radius)", desc = "Claim the chunk at your location", permission = "communities.normal.claim", max = 2, community = true, rank = CommunityRank.ASSISTANT)
public class ClaimChunk extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Player pSender = (Player) sender;
		Community community = player.getCommunity();
		if (args.length == 2) {
			int radius = Integer.parseInt(args[1]);

			if (radius > 0 && plugin.config().isValidChunkClaimRadius(radius)) {
				ClaimTask task = new ClaimMultiTask(community, pSender.getLocation(), radius);
				task.setSender(sender);
				Bukkit.getScheduler().scheduleAsyncDelayedTask(plugin, task);
			} else {
				throw new CommandException("Invalid chunk claim radius.");
			}
		} else {
			ClaimTask task = new ClaimSingleTask(community, pSender.getLocation());
			task.setSender(sender);
			Bukkit.getScheduler().scheduleAsyncDelayedTask(plugin, task);
		}
	}
}
