/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.command.module;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.betterverse.communities.Communities;
import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.WorldCoord;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;

/**
 *
 * @author Admin
 */
@AnnotatedCommand(name = "map", usage = "map", desc = "Display map of nearby area", permission = "communities.normal.map", min = 1, max = 4, community = false)
public class ShowMap extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Community community = player.getCommunity();
		float senderYaw = 0;

		WorldCoord coord;
		if (args.length >= 4) {
			String worldId = args[1];
			int x = Integer.parseInt(args[2]);
			int z = Integer.parseInt(args[3]);
			coord = new WorldCoord(worldId, x, z);
		} else if (sender instanceof Entity) {
			Entity entity = (Entity) sender;
			coord = WorldCoord.parseWorldCoord(entity);
			senderYaw = entity.getLocation().getYaw();
		} else {
			throw new CommandException("Must specify center chunk position. /map [world] [x] [z]");
		}

		CommunitiesAsciiMap map = new CommunitiesAsciiMap(plugin, sender, coord);
		map.setSenderCommunity(community);
		map.setSenderYaw(senderYaw);
		plugin.getServer().getScheduler().scheduleAsyncDelayedTask(plugin, new SendMap(map));
	}
}

class SendMap implements Runnable {

	CommunitiesAsciiMap map;

	public SendMap(CommunitiesAsciiMap map) {
		this.map = map;
	}

	@Override
	public void run() {
		map.sendToSender();
	}
}

class Compass {

	public enum Point {

		N, NE, E, SE, S, SW, W, NW
	}

	public static Compass.Point getCompassPointForDirection(double inDegrees) {

		double degrees = (inDegrees - 90) % 360;
		if (degrees < 0) {
			degrees += 360;
		}

		if (0 <= degrees && degrees < 22.5) {
			return Compass.Point.W;
		} else if (22.5 <= degrees && degrees < 67.5) {
			return Compass.Point.NW;
		} else if (67.5 <= degrees && degrees < 112.5) {
			return Compass.Point.N;
		} else if (112.5 <= degrees && degrees < 157.5) {
			return Compass.Point.NE;
		} else if (157.5 <= degrees && degrees < 202.5) {
			return Compass.Point.E;
		} else if (202.5 <= degrees && degrees < 247.5) {
			return Compass.Point.SE;
		} else if (247.5 <= degrees && degrees < 292.5) {
			return Compass.Point.S;
		} else if (292.5 <= degrees && degrees < 337.5) {
			return Compass.Point.SW;
		} else if (337.5 <= degrees && degrees < 360.0) {
			return Compass.Point.W;
		} else {
			return null;
		}
	}
}

abstract class AsciiMap {

	public static String[] generateCompass(float yaw) {

		Compass.Point dir = Compass.getCompassPointForDirection(yaw);

		return new String[]{
					ChatColor.BLACK + "  -----  ",
					ChatColor.BLACK + "  -" + (dir == Compass.Point.NW ? ChatColor.GOLD + "\\" : "-") + (dir == Compass.Point.N ? ChatColor.GOLD : ChatColor.WHITE) + "N" + (dir == Compass.Point.NE ? ChatColor.GOLD + "/" + ChatColor.BLACK : ChatColor.BLACK + "-") + "-  ",
					ChatColor.BLACK + "  -" + (dir == Compass.Point.W ? ChatColor.GOLD + "W" : ChatColor.WHITE + "W") + ChatColor.GRAY + "+" + (dir == Compass.Point.E ? ChatColor.GOLD : ChatColor.WHITE) + "E" + ChatColor.BLACK + "-  ",
					ChatColor.BLACK + "  -" + (dir == Compass.Point.SW ? ChatColor.GOLD + "/" : "-") + (dir == Compass.Point.S ? ChatColor.GOLD : ChatColor.WHITE) + "S" + (dir == Compass.Point.SE ? ChatColor.GOLD + "\\" + ChatColor.BLACK : ChatColor.BLACK + "-") + "-  "};
	}
	private int lineWidth;
	private int halfLineWidth;
	private int lineHeight;
	private int halfLineHeight;
	CommandSender sender;
	WorldCoord centerCoord;
	float senderYaw = 0;
	boolean usingMapTileWrapper = false;
	String mapTileWrapperLeft = "";
	String mapTileWrapperRight = "";
	ChatColor centerMapTileColor = ChatColor.GOLD;

	public AsciiMap(CommandSender sender, WorldCoord centerCoord) {
		this.sender = sender;
		this.centerCoord = centerCoord;
		setLineWidth(27);
		setLineHeight(7);
	}

	class MapTile {

		ChatColor characterColor;
		char character;
		ChatColor wrapperColor;

		public MapTile() {
		}

		public MapTile(ChatColor characterColor, char character, ChatColor wrapperColor) {
			this.characterColor = characterColor;
			this.character = character;
			this.wrapperColor = wrapperColor;
		}

		@Override
		public String toString() {
			return characterColor.toString() + character;
		}
	}

	public final void setLineWidth(int lineWidth) {
		this.lineWidth = lineWidth;
		halfLineWidth = lineWidth / 2;
	}

	public final void setLineHeight(int lineHeight) {
		this.lineHeight = lineHeight;
		halfLineHeight = lineHeight / 2;
	}

	public List<String> getHelp() {
		return new ArrayList<String>();
	}

	public void setMapTileWrapper(String leftSide, String rightSide) {
		mapTileWrapperLeft = leftSide;
		mapTileWrapperRight = rightSide;
		usingMapTileWrapper = !leftSide.isEmpty() || !rightSide.isEmpty();
	}

	public String formatMapTile(MapTile mapTile) {
		String wrapperColor = mapTile.wrapperColor.toString();
		return wrapperColor + mapTileWrapperLeft + mapTile + wrapperColor + mapTileWrapperRight;
	}

	public void setSenderYaw(float senderYaw) {
		this.senderYaw = senderYaw;
	}

	public MapTile getMapTileFor(WorldCoord coord) {
		MapTile mapTile = new MapTile();
		mapTile.characterColor = ChatColor.DARK_GRAY;
		mapTile.character = '-';
		mapTile.wrapperColor = ChatColor.DARK_GRAY;
		return mapTile;
	}

	public void sendToSender() {
		// Generate Map 
		MapTile[][] mapTiles = new MapTile[lineWidth][lineHeight];
		int x1 = centerCoord.getZ() - halfLineHeight;
		int x2 = centerCoord.getZ() + (lineHeight - halfLineHeight - 1);
		int y1 = centerCoord.getX() + (lineWidth - halfLineWidth - 1);
		int y2 = centerCoord.getX() - halfLineWidth;

		int x, y = 0;
		for (int tileY = y1; tileY >= y2; tileY--) {
			x = 0;
			for (int tileX = x1; tileX <= x2; tileX++) {
				mapTiles[y][x] = getMapTileFor(new WorldCoord(centerCoord.getWorldId(), tileY, tileX));

				// Color Center
				if (x == halfLineHeight && y == halfLineWidth) {
					if (usingMapTileWrapper) {
						mapTiles[y][x].wrapperColor = centerMapTileColor;
					} else {
						mapTiles[y][x].characterColor = centerMapTileColor;
					}
				}

				x++;
			}
			y++;
		}

		String[] compass = generateCompass(senderYaw);

		// Output
		String line;
		int lineCount = 0;
		// Variables have been rotated to fit N/S/E/W properly
		for (int my = 0; my < lineHeight; my++) {
			line = compass[0];
			if (lineCount < compass.length) {
				line = compass[lineCount];
			}

			String mapLine = "";
			for (int mx = lineWidth - 1; mx >= 0; mx--) {
				mapLine += formatMapTile(mapTiles[mx][my]);
			}
			line += mapLine;

			List<String> help = getHelp();

			if (lineCount < help.size()) {
				line += help.get(lineCount);
			}

			sender.sendMessage(line);
			lineCount++;
		}
	}
}

class CommunitiesAsciiMap extends AsciiMap {

	ChatColor defaultWrapperColor = ChatColor.BLACK;
	ChatColor unclaimedColor = ChatColor.DARK_GRAY;
	ChatColor claimedColor = ChatColor.WHITE;
	ChatColor ownTownColor = ChatColor.GREEN;
	char claimedChar = '+';
	char unclaimedChar = '-';
	Communities plugin;
	Community senderCommunity = null;

	public CommunitiesAsciiMap(Communities plugin, CommandSender sender, WorldCoord centerCoord) {
		super(sender, centerCoord);
		this.plugin = plugin;
		setMapTileWrapper("|", "|");
		setLineWidth(17);
	}

	public void setSenderCommunity(Community senderCommunity) {
		this.senderCommunity = senderCommunity;
	}

	@Override
	public MapTile getMapTileFor(WorldCoord coord) {
		MapTile mapTile = new MapTile();
		mapTile.wrapperColor = defaultWrapperColor;

		if (plugin.isChunkOwned(coord)) {
			mapTile.characterColor = claimedColor;
			mapTile.character = '+';

			if (senderCommunity != null && senderCommunity.ownsChunk(coord)) {
				mapTile.characterColor = ownTownColor;
			}
		} else {
			mapTile.characterColor = unclaimedColor;
			mapTile.character = '-';
		}

		return mapTile;
	}

	@Override
	public List<String> getHelp() {
		return Arrays.asList(
				"  " + formatMapTile(new MapTile(ChatColor.DARK_GRAY, '#', ChatColor.DARK_GRAY)) + ChatColor.GRAY + " = Chunk",
				"  " + formatMapTile(new MapTile(ChatColor.BLACK, '#', centerMapTileColor)) + ChatColor.GRAY + " = You",
				"  " + formatMapTile(new MapTile(unclaimedColor, unclaimedChar, defaultWrapperColor)) + ChatColor.GRAY + " = Unclaimed",
				"  " + formatMapTile(new MapTile(claimedColor, claimedChar, defaultWrapperColor)) + ChatColor.GRAY + " = Claimed",
				"  " + formatMapTile(new MapTile(ownTownColor, claimedChar, defaultWrapperColor)) + ChatColor.GRAY + " = Your town");
	}
}
