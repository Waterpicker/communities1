package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;

@AnnotatedCommand(name = "tax", usage = "tax <amount>", desc = "Set player tax for your town", permission = "communities.normal.tax", min = 2, max = 2, community = true, rank = CommunityRank.MAYOR)
public class SetTax extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		double amount = Double.parseDouble(args[1]);
		if (amount > 0) {
			player.getCommunity().setPlayerTax(amount);
			sender.sendMessage(ChatColor.GREEN + "Players will now be taxed " + ChatColor.YELLOW + plugin.economy().format(amount) + ChatColor.GREEN + ".");
		} else {
			throw new CommandException("You cannot set the player tax to a negative amount.");
		}
	}
}
