package net.betterverse.communities.command;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.Communities;
import net.betterverse.communities.community.CommunityPlayer;

public abstract class CommandModule {

	protected final Communities plugin;

	public CommandModule() {
		this.plugin = (Communities) Bukkit.getPluginManager().getPlugin("Communities");
	}

	public abstract void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException;

	public boolean canExecute() {
		return true;
	}
}
