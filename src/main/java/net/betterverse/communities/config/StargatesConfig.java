/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.config;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author Admin
 */
public class StargatesConfig extends YamlConfig {

	public static final String COMMUNITY_ID_PATTERN = "{community.id}";
	public static final int COMMUNITY_STARGATE_IDENTIFIER_SIGN_LINE_ID = 2; // Third line (aka network line).

	public StargatesConfig(File file) {
		super(file);
	}
	@Node("community_stargate.identifier")
	public String communityStargateIndentifier = "TOWN";
	@Node("community_stargate.allowed_gate_names")
	public List<String> allowedGateNames = Arrays.asList("nethergate");
	@Node("community_stargate.min_community_size")
	public int minCommunitySize = 10;
	@Node("community_stargate.max_network_size")
	public int maxCommunityNetworkSize = 10;
	@Node("community_stargate.network_name")
	public String communityStargateNetworkName = "TOWN" + COMMUNITY_ID_PATTERN;

	public boolean matchesCommunityNetworkNamePattern(String networkName) {
		String networkPattern = "^" + communityStargateNetworkName.replace(COMMUNITY_ID_PATTERN, "(\\d+)") + "$";
		return Pattern.compile(networkPattern, Pattern.CASE_INSENSITIVE).matcher(networkName).matches();
	}

	public String formatCommunityNetworkName(long communityId) {
		return communityStargateNetworkName.replace(COMMUNITY_ID_PATTERN, Long.toString(communityId));
	}
}
