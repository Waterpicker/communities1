/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Admin
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Node {

	String value();
}
