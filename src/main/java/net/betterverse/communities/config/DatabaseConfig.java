/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.config;

import java.io.File;
import net.betterverse.communities.community.DbLogin;

/**
 *
 * @author Admin
 */
public class DatabaseConfig extends YamlConfig {

	public DatabaseConfig(File file) {
		super(file);
	}
	@Node("url")
	public String url = "jdbc:h2:file:{rootpath}/data";
	@Node("username")
	public String username = "";
	@Node("password")
	public String password = "";

	public DbLogin getDbLogin(File rootFolder) {
		String address = this.url.replace("{rootpath}", rootFolder.getPath()); // Replace %s with root folder path
		String username = this.username.isEmpty() ? null : this.username; // Use null if empty
		String password = this.password.isEmpty() ? null : this.password; // Use null if empty
		return new DbLogin(address, username, password);
	}
}
