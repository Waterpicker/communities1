package net.betterverse.communities.community;

public enum CommunityRank {

	MAYOR(2),
	ASSISTANT(1),
	MEMBER(0),
	NONE(-1);
	private int level;

	private CommunityRank(int level) {
		this.level = level;
	}

	public int getLevel() {
		return level;
	}
}
