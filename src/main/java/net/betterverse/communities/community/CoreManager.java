/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.community;

import net.betterverse.communities.controller.CommunityStargateController;
import java.io.File;
import net.betterverse.communities.config.DatabaseConfig;
import net.betterverse.communities.config.StargatesConfig;
import net.betterverse.communities.controller.CommunityController;

/**
 *
 * @author Admin
 */
public class CoreManager {

	File rootFolder;
	DbManager dbManager;
	CommunityController communityController;
	CommunityStargateController communityStargateManager;
	DatabaseConfig databaseConfig;
	StargatesConfig communityStargateConfig;
	static String DATABASE_CONFIG_FILENAME = "config-database.yml";
	static String COMMUNITY_STARGATES_CONFIG_FILENAME = "config-stargates.yml";

	public CoreManager(File rootFolder) {
		setRootFolder(rootFolder);
		dbManager = new DbManager(this);
		communityController = new CommunityController(this);
		communityStargateManager = new CommunityStargateController(this);
	}

	private void setRootFolder(File rootFolder) {
		this.rootFolder = rootFolder;

		databaseConfig = new DatabaseConfig(new File(rootFolder, DATABASE_CONFIG_FILENAME));
		communityStargateConfig = new StargatesConfig(new File(rootFolder, COMMUNITY_STARGATES_CONFIG_FILENAME));
	}

	public File getRootFolder() {
		return rootFolder;
	}

	public DbManager getDbManager() {
		return dbManager;
	}

	public void load() throws Exception {
		loadConfig();
		getDbManager().setDbLogin(getDatabaseConfig().getDbLogin(getRootFolder()));
		loadData();
	}

	public void loadConfig() {
		databaseConfig.update();
		communityStargateConfig.update();
	}

	public void loadData() throws Exception {
		getDbManager().load();
	}

	public void unloadData() {
		getDbManager().unload();
	}

	public CommunityController getCommunityController() {
		return communityController;
	}

	public CommunityStargateController getCommunityStargateController() {
		return communityStargateManager;
	}

	public StargatesConfig getCommunityStargateConfig() {
		return communityStargateConfig;
	}

	public DatabaseConfig getDatabaseConfig() {
		return databaseConfig;
	}
}
