package net.betterverse.communities.community;

import java.io.File;

import net.betterverse.communities.Communities;
import net.betterverse.communities.util.Saveable;
import net.betterverse.communities.util.YamlFile;

import org.bukkit.event.Listener;

public class CommunityPlayer extends Saveable implements Listener {

	private final Communities plugin;
	private final String name;
	private Community community;
	private CommunityRank rank = CommunityRank.MEMBER;

	public CommunityPlayer(Communities plugin, String name) {
		super(new YamlFile(plugin, new File(plugin.getDataFolder() + File.separator + "players", name + ".yml"), name));
		this.plugin = plugin;
		this.name = name;
	}

	@Override
	public void load() {
		rank = CommunityRank.valueOf(getString("rank", "NONE"));
		if (getString("community") != null && !getString("community").isEmpty()) {
			Community community = plugin.getCommunity(getString("community"));
			community.addPlayer(name, this);
			this.community = community;
		}
	}

	@Override
	public void save() {
		set("community", community == null ? "" : community.getName());
		set("rank", rank.name());
	}

	public Community getCommunity() {
		return community;
	}

	public String getName() {
		return name;
	}

	public CommunityRank getRank() {
		return rank;
	}

	public boolean isLeader() {
		return rank.getLevel() >= CommunityRank.ASSISTANT.getLevel();
	}

	public boolean isMayor() {
		return rank == CommunityRank.MAYOR;
	}

	public void joinCommunity(Community community, CommunityRank rank) {
		this.community = community;
		set("community", community.getName());
		setRank(rank);
	}

	public void leaveCommunity() {
		community = null;
		set("community", "");
		setRank(CommunityRank.NONE);
	}

	public void setRank(CommunityRank rank) {
		this.rank = rank;
		set("rank", rank.name());
	}

	public boolean charge(int salePrice) {
		if(!(plugin.economy().has(name, salePrice))) {
			return false;
		}
		plugin.economy().withdrawPlayer(name, salePrice);
		return true;
	}
	
	public Communities getPlugin() {
		return plugin;
	}
}
