package net.betterverse.communities.community;

import java.util.HashSet;
import java.util.Set;

import net.betterverse.communities.Communities;
import net.betterverse.communities.util.StringHelper;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;

public class CommunityChunkGroup {

	private final Set<CommunityChunk> children = new HashSet<CommunityChunk>();
	private final Set<EntityType> canSpawn = new HashSet<EntityType>();
	private final String name;
	private final ConfigurationSection section;
	private String description;
	private boolean pvp;
	private boolean explosions;
	private boolean fireSpread;
	private boolean monsters;
	private String owner;
	private int onSale;

	public CommunityChunkGroup(Communities plugin, String name, Community parent) {
		this.name = name;
		this.description = "This is the default chunk group description. Use '/coms description' to change it.";

		section = parent.getSection("chunk-groups." + name);

		pvp = plugin.config().canPvP();
		explosions = plugin.config().areExplosionsAllowed();
		fireSpread = plugin.config().canFireSpread();
	}

	public void addChild(CommunityChunk chunk) {
		children.add(chunk);
	}

	public boolean areExplosionsAllowed() {
		return explosions;
	}

	public boolean canFireSpread() {
		return fireSpread;
	}

	public boolean canMobSpawn(EntityType type) {
		return canSpawn.contains(type);
	}

	public String getDescription() {
		return StringHelper.parseColors(description);
	}

	public String getName() {
		return name;
	}

	public boolean isPvPAllowed() {
		return pvp;
	}

	public void load() {
		description = section.getString("description");
		pvp = section.getBoolean("pvp");
		explosions = section.getBoolean("explosions");
		fireSpread = section.getBoolean("fire-spread");
		owner = section.getString("owner");
		onSale = section.getInt("on-sale");
		for (String mob : section.getStringList("spawnable")) {
			EntityType entityType = EntityType.fromName(mob);
			if (entityType != null) {
				canSpawn.add(entityType);
			}
		}
	}

	public void save() {
		section.set("description", description);
		section.set("pvp", pvp);
		section.set("explosions", explosions);
		section.set("fire-spread", fireSpread);
		section.set("owner", owner);
		section.set("on-sale", onSale);

		// Convert spawnables to strings
		Set<String> converted = new HashSet<String>();
		for (EntityType spawn : canSpawn) {
			converted.add(spawn.name());
		}
		section.set("spawnable", converted.toArray());
	}

	public boolean toggleMonsters() {
		for (CommunityChunk child : children) {
			child.toggleMonsters();
		}

		save();

		return monsters = !monsters;
	}

	public String getOwner() {
		return owner;
	}

	public boolean hasOwner() {
		return owner != null;
	}

	public void setOwner(String newOwner) {
		owner = newOwner;
	}

	public boolean isOnSale() {
		return onSale != 0;
	}

	public void setOnSale(int nS) {
		onSale = nS;
	}
	
	public int getSalePrice() {
		return onSale;
	}
}
