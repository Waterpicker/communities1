/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.community;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;

/**
 *
 * @author Admin
 */
public class WorldCoord extends Coord {

	protected String worldId;

	public WorldCoord(String worldId, int x, int z) {
		super(x, z);
		this.worldId = worldId;
	}

	public WorldCoord(String worldId, Coord coord) {
		super(coord);
		this.worldId = worldId;
	}

	public WorldCoord(WorldCoord worldCoord) {
		super(worldCoord);
		this.worldId = worldCoord.getWorldId();
	}

	public String getWorldId() {
		return worldId;
	}

	public Coord getCoord() {
		return new Coord(x, z);
	}

	public static WorldCoord parseWorldCoord(Entity entity) {
		return parseWorldCoord(entity.getLocation());
	}

	public static WorldCoord parseWorldCoord(Location loc) {
		return new WorldCoord(loc.getWorld().getName(), parseCoord(loc));
	}

	public static WorldCoord parseWorldCoord(Block block) {
		return new WorldCoord(block.getWorld().getName(), parseCoord(block.getX(), block.getZ()));
	}

	public WorldCoord add(int xOffset, int zOffset) {
		return new WorldCoord(getWorldId(), getX() + xOffset, getZ() + zOffset);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(x)
				.append(z)
				.append(worldId)
				.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof WorldCoord) {
			WorldCoord that = (WorldCoord) obj;
			return new EqualsBuilder()
					.append(this.x, that.x)
					.append(this.z, that.z)
					.append(this.worldId, that.worldId)
					.isEquals();
		} else if (obj instanceof Coord) {
			return super.equals(obj);
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return worldId + "," + super.toString();
	}

	/**
	 * Shortcut for Bukkit.getWorld(worldName)
	 *
	 * @return the relevant org.bukkit.World instance
	 */
	public World getBukkitWorld() {
		return Bukkit.getWorld(worldId);
	}
}
