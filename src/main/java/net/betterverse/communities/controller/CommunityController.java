/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.controller;

import com.iciql.Db;
import java.util.List;
import net.betterverse.communities.community.CoreManager;
import net.betterverse.communities.model.Community;
import net.betterverse.communities.model.CommunityStargate;

/**
 *
 * @author Admin
 */
public class CommunityController extends Controller {

	public CommunityController(CoreManager core) {
		super(core);
	}

	public List<CommunityStargate> getCommunityStargates(Community community) {
		Db db = getDb();
		CommunityStargate cs = new CommunityStargate(); // Not Thread Safe.
		return db.from(cs).where(cs.communityId).is(community.id).select();
	}

	public Community getByName(String name) {
		Db db = getDb();
		Community c = new Community(); // Not Thread Safe.
		return db.from(c).where(c.name).is(name).selectFirst();
	}

	public String getCommunityPortalNetwork(Community community) {
		return getCore().getCommunityStargateConfig().formatCommunityNetworkName(community.id);
	}

	public Community add(Community community) {
		Db db = getDb();
		long id = db.insertAndGetKey(community);
		Community c = new Community();
		return db.from(c).where(c.id).is(id).selectFirst();
	}

	public Community getById(long communityId) {
		Db db = getDb();
		Community c = new Community(); // Not Thread Safe.
		return db.from(c).where(c.id).is(communityId).selectFirst();
	}

	public Community create(String name) {
		Community community = new Community();
		community.name = name;
		return add(community);
	}

	public void delete(Community community) {
		Db db = getDb();
		db.delete(community);
	}
}
