/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.model;

import com.iciql.Iciql.IQColumn;
import com.iciql.Iciql.IQTable;

/**
 *
 * @author Admin
 */
@IQTable
public class CommunityStargate {

	@IQColumn(primaryKey = true, autoIncrement = true)
	public long id;
	@IQColumn(nullable = false, length = 16)
	public String portalName;
	@IQColumn(nullable = false, length = 16)
	public String networkName;
	@IQColumn()
	public long communityId;

	@Override
	public String toString() {
		return String.format("CommunityStargate(%d, %s, network=%s, communityId=%d)", id, portalName, networkName, communityId);
	}
}
