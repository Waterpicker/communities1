/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.task;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import net.betterverse.communities.community.WorldCoord;
import net.betterverse.communities.event.CommunityClaimChunkEvent;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.util.Vector;

/**
 *
 * @author Admin
 */
public abstract class SelectionTask implements Runnable, ChunkSelection {

	Set<WorldCoord> selection = new LinkedHashSet<WorldCoord>(); // Preserve Order
	World cursorWorld;
	Vector cursorVector;
	CommandSender sender = null;
	int removed = 0;
	int attempted = 0;
	int failed = 0;
	int succceded = 0;

	public void setSender(CommandSender sender) {
		this.sender = sender;
	}

	public CommandSender getSender() {
		return sender;
	}

	public void removeFromSelection(WorldCoord coord) {
		selection.remove(coord);
		removed++;
	}

	public Set<WorldCoord> getSelection() {
		return selection;
	}

	public void sendToSender(String msg, Object... args) {
		if (sender != null) {
			sender.sendMessage(String.format(msg, args));
		}
	}

	public void addToSelection(WorldCoord coord) {
		selection.add(coord);
	}

	public void addToSelection(Collection<WorldCoord> coords) {
		selection.addAll(coords);
	}

	public void run() {
		loadSelection();
	}

	public abstract void loadSelection();

	//TODO
	public Set<WorldCoord> filterUngeneratedChunks(Set<WorldCoord> coords) {
		Set<WorldCoord> filteredWorldCoords = new HashSet<WorldCoord>();
		for (WorldCoord coord : coords) {
			// TODO
			if (cursorWorld.isChunkLoaded(coord.getX(), coord.getZ())) {
				filteredWorldCoords.add(coord);
			} else {
				removed++;
			}
		}
		return filteredWorldCoords;
	}
}
