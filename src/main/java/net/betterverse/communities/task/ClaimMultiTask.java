/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.task;

import java.util.*;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.Coord;
import net.betterverse.communities.community.WorldCoord;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Location;

/**
 *
 * @author Admin
 */
public class ClaimMultiTask extends ClaimTask {

	int radius;

	public ClaimMultiTask(Community community, Location cursorLocation, int radius) {
		super(community, cursorLocation);
		this.radius = radius;
	}

	public void sendResults() {
		if (succceded > 0) {
			sendToSender(ChatColor.GREEN + "Successfully claimed " + ChatColor.YELLOW + "%d" + ChatColor.GREEN + " chunks within a " + ChatColor.YELLOW + "%d" + ChatColor.GREEN + " block radius.",
					succceded,
					radius);
		}
		if (failed > 0) {
			sendToSender(ChatColor.RED + "Failed to claim %d chunks.",
					failed);
		}
	}

	@Override
	public void loadSelection() {
		addToSelection(getCircleAround(cursorWorld.getName(), cursorVector.getBlockX(), cursorVector.getBlockZ(), radius));
	}

	public static Set<WorldCoord> getSpiralingFrom(Location centerLoc, int radius) {
		return getSpiralingFrom(centerLoc.getWorld().getName(), centerLoc.getBlockX(), centerLoc.getBlockZ(), radius);
	}

	/**
	 * Produces a spiraling effect Eg: http://pastie.org/4345567
	 */
	public static Set<WorldCoord> getSpiralingFrom(String worldId, int centerX, int centerZ, int radius) {
		Set<WorldCoord> coords = new LinkedHashSet<WorldCoord>();

		int x = centerX;
		int z = centerZ;

		int scale = 16;

		// Direction to move search
		int dx = scale;
		int dz = 0;

		int segmentLength = scale; // Length of search segment
		int segmentPassed = 0; // Amount of segment that has been searched through
		for (int i = 0; i < ((radius * 2) ^ 2); ++i) {
			x += dx;
			z += dz;
			segmentPassed += scale;

			if (segmentPassed == segmentLength) {
				segmentPassed = 0;

				// Rotate Arm
				int buffer = dx;
				dx = -dz;
				dz = buffer;

				// Check for complete rotation.
				if (dz == 0) {
					// Lengthen arm.
					segmentLength += scale;
				}

				WorldCoord coord = new WorldCoord(worldId, Coord.parseCoord(x, z));
				coords.add(coord);
			}
		}
		return coords;
	}

	public static Set<WorldCoord> getCircleAround(String worldId, int startX, int startZ, int radius) {
		class Vector2d {

			int x;
			int z;

			public Vector2d(int x, int z) {
				this.x = x;
				this.z = z;
			}

			public Vector2d add(int x, int z) {
				return new Vector2d(this.x + x, this.z + z);
			}

			public Collection<Vector2d> getNeighbours(int w) {
				Collection<Vector2d> neighbours = new ArrayList<Vector2d>();
				neighbours.add(add(w, 0));
				neighbours.add(add(0, w));
				neighbours.add(add(-w, 0));
				neighbours.add(add(0, -w));
				return neighbours;
			}

			public double distanceSquared(Vector2d o) {
				return Math.pow(x - o.x, 2) + Math.pow(z - o.z, 2);
			}

			@Override
			public String toString() {
				return String.format("(%d, %d)", x, z);
			}

			@Override
			public boolean equals(Object obj) {
				if (obj instanceof Vector2d) {
					Vector2d that = (Vector2d) obj;
					return new EqualsBuilder()
							.append(this.x, that.x)
							.append(this.z, that.z)
							.isEquals();
				} else {
					return false;
				}
			}

			@Override
			public int hashCode() {
				return new HashCodeBuilder()
						.append(x)
						.append(z)
						.toHashCode();
			}
		}

		Vector2d start = new Vector2d(startX, startZ);
		int scale = 16;

		radius += scale - (radius % scale);

		int radiusSquared = radius * radius;

		Queue<Vector2d> frontier = new LinkedList<Vector2d>();
		Collection<Vector2d> visited = new ArrayList<Vector2d>();
		Set<WorldCoord> selection = new LinkedHashSet<WorldCoord>();

		frontier.add(start);

		while (!frontier.isEmpty()) {
			Vector2d pos = frontier.poll();
			visited.add(pos);

			//
			double dist = pos.distanceSquared(start);
			if (dist > radiusSquared) {
				continue;
			}

			selection.add(new WorldCoord(worldId, Coord.parseCoord(pos.x, pos.z)));

			// Expand
			for (Vector2d neighbour : pos.getNeighbours(scale)) {
				if (visited.contains(neighbour)) {
					continue;
				}
				if (frontier.contains(neighbour)) {
					continue;
				}

				frontier.add(neighbour);
			}
		}
		return selection;
	}
}
