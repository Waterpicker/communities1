/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.task;

import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.WorldCoord;
import net.betterverse.communities.event.CommunityClaimChunkEvent;
import net.betterverse.communities.event.CommunityUnclaimChunkEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 *
 * @author Admin
 */
public abstract class UnclaimTask extends SelectionTask {

	Community community;

	public UnclaimTask(Community community, Location cursorLocation) {
		this.community = community;
		this.cursorWorld = cursorLocation.getWorld();
		this.cursorVector = cursorLocation.toVector();
	}

	public Community getCommunity() {
		return community;
	}

	public void sendResults() {
		sendToSender("Removed (Filtered Out): %d", removed);
		sendToSender("Attempted: %d", attempted);
		sendToSender("Succeded: %d", succceded);
		sendToSender("Failed: %d", failed);
	}

	@Override
	public void run() {
		loadSelection();

		if (Bukkit.getPluginManager() != null) {
			CommunityUnclaimChunkEvent event = new CommunityUnclaimChunkEvent(this);
			Bukkit.getPluginManager().callEvent(event);
			if (event.isCancelled()) {
				return;
			}
		}

		attemptToUnclaimSelection();
		sendResults();
	}

	public void attemptToUnclaimSelection() {
		for (WorldCoord coord : selection) {
			attempted++;
			community.unclaimChunk(coord);
			succceded++;
		}

		failed = selection.size() - succceded;
	}
}
