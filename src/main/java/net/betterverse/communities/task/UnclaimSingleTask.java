/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.task;

import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.Coord;
import net.betterverse.communities.community.WorldCoord;
import org.bukkit.ChatColor;
import org.bukkit.Location;

/**
 *
 * @author Admin
 */
public class UnclaimSingleTask extends UnclaimTask implements SingleChunkSelection {

	public UnclaimSingleTask(Community community, Location cursorLocation) {
		super(community, cursorLocation);
	}

	public WorldCoord getFirstSelection() {
		return selection.iterator().next();
	}

	@Override
	public void loadSelection() {
		WorldCoord coord = new WorldCoord(cursorWorld.getName(), Coord.parseCoord(cursorVector.getBlockX(), cursorVector.getBlockZ()));
		addToSelection(coord);
	}

	@Override
	public void sendResults() {
		if (succceded > 0) {
			sendToSender(ChatColor.GREEN + "Unclaimed the chunk at your location.");
		}
	}
}
