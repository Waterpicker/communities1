/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.task;

import java.util.Collection;
import java.util.Set;
import net.betterverse.communities.community.WorldCoord;

/**
 *
 * @author Admin
 */
public interface ChunkSelection {

	public Set<WorldCoord> getSelection();

	public void addToSelection(WorldCoord coord);

	public void addToSelection(Collection<WorldCoord> coords);

	public void removeFromSelection(WorldCoord coord);
}
