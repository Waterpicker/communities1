/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.event;

import java.util.Set;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.WorldCoord;
import net.betterverse.communities.task.UnclaimTask;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * @author Admin
 */
public class CommunityUnclaimChunkEvent extends Event implements Cancellable {

	private static final HandlerList handlers = new HandlerList();
	private boolean cancelled = false;
	private UnclaimTask unclaimTask;

	public CommunityUnclaimChunkEvent(UnclaimTask unclaimTask) {
		this.unclaimTask = unclaimTask;
	}

	public Community getCommunity() {
		return getUnclaimTask().getCommunity();
	}

	public CommandSender getSender() {
		return getUnclaimTask().getSender();
	}

	public UnclaimTask getUnclaimTask() {
		return unclaimTask;
	}

	public Set<WorldCoord> getSelection() {
		return getUnclaimTask().getSelection();
	}

	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean cancel) {
		this.cancelled = cancel;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}