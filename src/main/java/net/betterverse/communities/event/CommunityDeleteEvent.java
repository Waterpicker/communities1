/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.event;

import net.betterverse.communities.community.Community;
import org.bukkit.event.HandlerList;

/**
 *
 * @author Admin
 */
public class CommunityDeleteEvent extends CommunityEvent {

	private static final HandlerList handlers = new HandlerList();

	public CommunityDeleteEvent(Community community) {
		super(community);
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
