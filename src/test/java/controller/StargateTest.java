/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import net.betterverse.communities.community.CoreManager;
import net.betterverse.communities.community.DbLogin;
import net.betterverse.communities.controller.CommunityController;
import net.betterverse.communities.model.Community;
import net.betterverse.communities.model.CommunityStargate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Admin
 */
public class StargateTest {

	CoreManager core;

	@BeforeClass
	public static void setUpClass() throws Exception {
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		File rootFolder = new File("target/test-root");
		System.out.println(rootFolder.getAbsolutePath());
		core = new CoreManager(rootFolder);
		core.loadConfig();
		core.getDbManager().setDbLogin(new DbLogin(String.format("jdbc:h2:file:%s/testdbs/h2/communities", core.getRootFolder().getPath())));
		core.loadData();
		core.getDbManager().getDb().activateConsoleLogger();
		core.getDbManager().resetDb();
	}

	@After
	public void tearDown() {
		core.unloadData();
	}

	@Test
	public void test() {
		String communityName = "toronto";
		String portalName = "tower";

		CommunityController communityController = core.getCommunityController();
		//TODO Refactor when moved to sql everywhere.
		Community community = communityController.getByName(communityName);
		if (community == null) {
			community = communityController.create(communityName);
		}
		String networkName = communityController.getCommunityPortalNetwork(community);

		CommunityStargate communityStargate = new CommunityStargate();
		communityStargate.communityId = community.id;
		communityStargate.portalName = portalName;
		communityStargate.networkName = networkName;
		try {
			core.getCommunityStargateController().validate(communityStargate);
			core.getCommunityStargateController().register(communityStargate);
		} catch (Exception e) {
			Assert.fail("Error validating the first time.");
		}

		try {
			// Validation should fail if it was successfully registered.
			core.getCommunityStargateController().validate(communityStargate);
			Assert.fail("Validation passed the second time when it should fail.");
		} catch (Exception e) {
		}

		Assert.assertEquals(communityController.getCommunityStargates(community).size(), 1);
	}
}
